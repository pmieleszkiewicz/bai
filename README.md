# Projects App

Aplikacja na BAI. Umożliwia tworzenie i zarządzanie projektami.

## Instalacja
PyCharm powinien sam wykryć plik Pipfile i uruchomić wirtualne środowisko.

```bash
pipenv install
pipenv shell
cd src
python manage.py migrate
python manage.py runserver
```
