import hashlib
import random


def generate_password_fragments(raw_password):
    fragments = []
    indexes = set()
    min_frag_len = 5
    max_frag_len = len(raw_password) // 2
    print(min_frag_len, max_frag_len)
    if max_frag_len < min_frag_len:
        frag_len = min_frag_len
    else:
        frag_len = random.randint(min_frag_len, max_frag_len)
    print(frag_len)

    for i in range(10):
        while len(indexes) < len(raw_password) - frag_len:
            indexes.add(random.randint(0, len(raw_password) - 1))

        fragment = list(raw_password)
        for index in indexes:
            fragment[index] = '*'

        fragment = ''.join(fragment)
        fragment = hashlib.sha224(fragment.encode()).hexdigest()

        fragments.append((fragment, list(indexes)))
        indexes = set()

    fragments.append(len(raw_password))
    return fragments
