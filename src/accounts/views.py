import hashlib
from django.contrib import messages
from django.contrib.auth import authenticate, login, views
from django.shortcuts import render, redirect
from django.utils import timezone
from django.views import View

from .forms import CustomLoginForm
from .models import User
from .utils import generate_password_fragments


class PasswordChangeView(views.PasswordChangeView):
    def form_valid(self, form):
        new_password = form.cleaned_data['new_password1']
        password_fragments = generate_password_fragments(new_password)
        self.request.user.password_fragments = password_fragments
        return super().form_valid(form)


class CustomLoginView(View):
    template_name = 'registration/login.html'

    def get(self, *args, **kwargs):
        email = self.request.GET.get('username')
        try:
            user = User.objects.get(email=email)
            password_length = user.password_fragments[-1]
            indexes_to_disable = user.password_fragments[user.password_fragment_id][1]
        except User.DoesNotExist:
            password_length = 8
            indexes_to_disable = [1, 3, 5]

        if email:
            form = CustomLoginForm(initial={'username': email,
                                            'password': ''},
                                   password_length=password_length,
                                   indexes_to_disable=indexes_to_disable)
        else:
            form = CustomLoginForm(initial={'username': email})
        return render(self.request, self.template_name, {'form': form})

    def post(self, *args, **kwargs):
        pwd_len = 0
        for key in self.request.POST:
            if key.startswith('password'):
                pwd_len += 1

        form = CustomLoginForm(self.request.POST, password_length=pwd_len, indexes_to_disable=[1])

        if form.is_valid():
            user = None
            try:
                user = User.objects.get(email=form.cleaned_data['username'])
            except User.DoesNotExist:
                messages.error(self.request, 'Invalid credentials')
                return redirect('login')

            if user.blocked_to and timezone.now() < user.blocked_to:
                time_left = user.blocked_to - timezone.now()
                messages.error(self.request,
                               f'You account has been blocked! Wait {round(time_left.total_seconds())} seconds before next attempt.')
                return redirect('login')

            hashed_pwd = hashlib.sha224(form.cleaned_data['password'].encode()).hexdigest()
            user = authenticate(username=user.email, hash=hashed_pwd)

            if user:
                login(self.request, user)
                return redirect('projects_list')
            else:
                messages.error(self.request, 'Invalid credentials')
                return redirect('login')
        else:
            messages.error(self.request, 'Invalid credentials')
            return redirect('login')


class ProfileView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'accounts/profile_detail.html', {'user': self.request.user})