from django import forms
from django.core.validators import MaxLengthValidator
from django.forms import MultiValueField, CharField


class PasswordMultiWidget(forms.MultiWidget):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def decompress(self, value):
        return tuple(value)


class PasswordMultiField(MultiValueField):
    def __init__(self, **kwargs):
        error_messages = {
            'incomplete': 'Enter a valid password',
            'required': 'Enter a required password',
            'invalid': 'Enter a invalid password',
        }
        fields = tuple([CharField(validators=[MaxLengthValidator(1)]) for i in range(kwargs['length'])])
        kwargs.pop('length')
        super().__init__(
            error_messages=error_messages, fields=fields,
            require_all_fields=False, **kwargs
        )

    def compress(self, data_list):
        print(data_list)
        return ''.join(data_list)


# class CustomAuthForm(AuthenticationForm):
#     def __init__(self, password_length=None, indexes_to_disable=None, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         self.password_length = password_length
#         print(f'Created form with password: {self.password_length}')
#         print(indexes_to_disable)
#         if password_length:
#             print(password_length)
#             print(indexes_to_disable)
#             _widgets = []
#             for i in range(password_length):
#                 if i in indexes_to_disable:
#                     _widgets.append(forms.TextInput(attrs={'class': 'form-control', 'type': 'password', 'maxlength': 1, 'size': 1, 'disabled': True, 'value': '*'}))
#                 else:
#                     _widgets.append(forms.TextInput(attrs={'class': 'form-control', 'type': 'password',  'maxlength': 1, 'size': 1}))
#             self.fields['password'] = PasswordMultiField(widget=PasswordMultiWidget(widgets=_widgets), length=password_length)


# class CustomAuthFormWithoutPwd(AuthenticationForm):
#     password = None


class CustomLoginForm(forms.Form):
    username = forms.EmailField(label='Email', max_length=64, required=True)

    def __init__(self, *args, **kwargs):
        password_length = kwargs.pop('password_length') if kwargs.get('password_length') else None
        indexes_to_disable = kwargs.pop('indexes_to_disable') if kwargs.get('indexes_to_disable') else None

        super().__init__(*args, **kwargs)
        if password_length:
            _widgets = []
            for i in range(password_length):
                if i in indexes_to_disable:
                    _widgets.append(forms.TextInput(attrs={'class': 'form-control', 'type': 'password', 'maxlength': 1, 'size': 1, 'disabled': True, 'value': '*'}))
                else:
                    _widgets.append(forms.TextInput(attrs={'class': 'form-control', 'type': 'password',  'maxlength': 1, 'size': 1}))
            self.fields['password'] = PasswordMultiField(widget=PasswordMultiWidget(widgets=_widgets), length=password_length)



