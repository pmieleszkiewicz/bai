from django.contrib.auth.backends import BaseBackend

from .models import User


class HashModelBackend(BaseBackend):
    def authenticate(self, request, username=None, hash=None):
        try:
            user = User.objects.get(email=username)
            fragment = user.password_fragments[user.password_fragment_id][0]
            if fragment == hash:
                return user
            return None
        except User.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None