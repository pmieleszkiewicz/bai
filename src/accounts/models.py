import hashlib
from datetime import timedelta

from django.contrib.auth.signals import user_logged_in, user_logged_out, user_login_failed
from django.contrib.auth.base_user import BaseUserManager
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import signals
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
from django.contrib.postgres.fields import JSONField

from .utils import generate_password_fragments


class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.raw_password = password
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
    username = None
    email = models.EmailField(_('email address'), unique=True)
    password_fragments = JSONField(null=False, default=list)
    password_fragment_id = models.SmallIntegerField(
        default=0,
        validators=[
            MaxValueValidator(9),
            MinValueValidator(0)
        ]
    )


    objects = UserManager()

    invalid_login_at = models.DateTimeField(null=True)
    valid_login_at = models.DateTimeField(null=True)
    blocked_to = models.DateTimeField(null=True)
    invalid_logins_number = models.IntegerField(default=0)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    MAX_LOGIN_ATTEMPTS = 3


# Login listeners
@receiver(user_logged_in)
def user_logged_in_callback(sender, request, user, **kwargs):
    user.valid_login_at = timezone.now()
    user.blocked_to = None
    user.password_fragment_id = 0 if user.password_fragment_id == 9 else user.password_fragment_id + 1
    user.save()


@receiver(user_logged_out)
def user_logged_out_callback(sender, request, user, **kwargs):
    user.invalid_logins_number = 0
    user.save()


@receiver(user_login_failed)
def user_login_failed_callback(sender, credentials, **kwargs):
    try:
        user = User.objects.get(email=credentials['username'])
    except User.DoesNotExist:
        user = None

    if user:
        user.invalid_login_at = timezone.now()
        user.invalid_logins_number += 1
        # hard-coded invalid login attempts
        if user.invalid_logins_number >= user.MAX_LOGIN_ATTEMPTS:
            seconds_to_add = user.invalid_logins_number * 20
            if user.blocked_to:
                user.blocked_to = user.blocked_to + timedelta(seconds=seconds_to_add)
            else:
                user.blocked_to = timezone.now() + timedelta(seconds=seconds_to_add)
        user.save()


@receiver(signals.post_save, sender=User)
def user_post_save(sender, instance, created, *args, **kwargs):
    if created:
        raw_password = instance._password
        password_fragments = generate_password_fragments(raw_password)
        instance.password_fragments = password_fragments
        instance.save()
