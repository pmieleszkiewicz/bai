from django.contrib import admin
from django.urls import path, include

from projects.views import ProjectListView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('accounts.urls')),
    path('projects/', include('projects.urls')),
    path('', ProjectListView.as_view(), name='home'),
]
