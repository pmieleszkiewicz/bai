from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import generic
from django.contrib.auth.decorators import login_required

from .forms import ProjectForm
from .models import Project


@method_decorator(login_required, name='dispatch')
class ProjectListView(generic.ListView):
    model = Project
    template_name = 'projects/project/project_list.html'
    context_object_name = 'projects'
    ordering = ['-created_at']

    def get_queryset(self):
        return Project.objects.filter(Q(owner=self.request.user) | Q(members__id=self.request.user.id)).distinct()


@method_decorator(login_required, name='dispatch')
class ProjectCreateView(generic.CreateView):
    model = Project
    template_name = 'projects/project/project_create.html'
    form_class = ProjectForm
    success_url = reverse_lazy('projects_list')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.owner = self.request.user
        self.object.save()
        return super().form_valid(form)


@method_decorator(login_required, name='dispatch')
class ProjectDetailView(generic.DetailView):
    model = Project
    template_name = 'projects/project/project_detail.html'
    context_object_name = 'project'

    def get_object(self, queryset=None):
        obj = super().get_object()
        if not obj.owner == self.request.user and not self.request.user in obj.members.all():
            raise PermissionDenied
        return obj


@method_decorator(login_required, name='dispatch')
class ProjectDeleteView(generic.DeleteView):
    model = Project
    success_url = reverse_lazy('projects_list')

    def get_object(self, queryset=None):
        obj = super().get_object()
        if not obj.owner == self.request.user:
            raise PermissionDenied
        return obj


@method_decorator(login_required, name='dispatch')
class ProjectUpdateView(generic.UpdateView):
    model = Project
    success_url = reverse_lazy('projects_list')
    form_class = ProjectForm
    template_name = 'projects/project/project_update.html'
