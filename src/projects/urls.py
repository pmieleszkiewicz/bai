from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.ProjectListView.as_view(), name='projects_list'),
    path('create/', views.ProjectCreateView.as_view(), name='projects_create'),
    path('<int:pk>', views.ProjectDetailView.as_view(), name='projects_detail'),
    path('<int:pk>/delete', views.ProjectDeleteView.as_view(), name='projects_delete'),
    path('<int:pk>/update', views.ProjectUpdateView.as_view(), name='projects_update'),
]
